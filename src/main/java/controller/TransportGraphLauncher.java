package controller;

import graphalgorithms.*;
import model.TransportGraph;

public class TransportGraphLauncher {

    public static void main(String[] args) {
        String[] redLine = {"red", "metro", "A", "B", "C", "D"};
        String[] blueLine = {"blue", "metro", "E", "B", "F", "G"};
        String[] greenLine = {"green", "metro", "H", "I", "C", "G", "J"};
        String[] yellowLine = {"yellow", "bus", "A", "E", "H", "D", "G", "A"};


        String[] red = {"red","metro","Haven", "Marken", "Steigerplein", "Centrum", "Meridiaan", "Dukdalf", "Oostvaarders"};
        Integer[] redCoordinates = {14,1,12,3,10,5,8,8,6,9,3,10,0,11};
        Double[] redWeights = {4.5,4.7,6.1,3.5,5.4,5.6};
        String[] blue = {"blue","metro","Trojelaan", "Coltrane Cirkel", "Meridiaan", "Robijnpark", "Violetplantsoen"};
        Integer[] blueCoordinates = {9,3,7,6,6,9,6,12,5,14};
        Double[] blueWeights = {6.0,5.3,5.1,3.3};
        String[] purple = {"purple","metro","Grote Sluis", "Grootzeil", "Coltrane Cirkel", "Centrum", "Swingstraat"};
        Integer[] purpleCoordinates = {2,3,4,6,7,6,8,8,10,9};
        Double[] purpleWeights = {6.2,5.2,3.8,3.6};
        String[] green = {"green","metro","Ymeerdijk", "Trojelaan", "Steigerplein", "Swingstraat", "Bachgracht", "Nobelplein"};
        Double[] greenWeights = {5.0,3.7,6.9,3.9,3.4};
        Integer[] greenCoordinates = {9,0,9,3,10,5,10,9,11,11,12,13};
        String[] yellow = {"yellow","bus","Grote Sluis", "Ymeerdijk", "Haven", "Nobelplein", "Violetplantsoen", "Oostvaarders", "Grote Sluis"};
        Double[] yellowWeights = {26.0,19.0,37.0,25.0,22.0,28.0};
        Integer[] yellowCoordinates = {2,3,9,0,14,1,12,13,5,14,0,11,2,3};

        // TODO Use the builder to build the graph from the String array.

//        Uncomment to test the builder:
//        System.out.println(transportGraph);
        TransportGraph.Builder builder = new TransportGraph.Builder();
        builder.addLine(red,redWeights,redCoordinates);
        builder.addLine(blue,blueWeights,blueCoordinates);
        builder.addLine(purple,purpleWeights,purpleCoordinates);
        builder.addLine(green,greenWeights,greenCoordinates);
        builder.addLine(yellow,yellowWeights,yellowCoordinates);

//        builder.addLine(redLine);
//        builder.addLine(blueLine);
//        builder.addLine(greenLine);
//        builder.addLine(yellowLine);

        builder.buildStationSet();
        builder.addLinesToStations();
        builder.buildConnections();
        TransportGraph graph = builder.build();
//        System.out.println(graph.toString());


//        Uncommented to test the DepthFirstPath algorithm
//        DepthFirstPath dfpTest = new DepthFirstPath(graph, "Haven", "Violetplantsoen");
//        dfpTest.search();
//        System.out.println(dfpTest);
//        dfpTest.printNodesInVisitedOrder();

//        Uncommented to test the BreadthFirstPath algorithm
//        BreadthFirstPath bfsTest = new BreadthFirstPath(graph,  "Haven", "Violetplantsoen");
//        bfsTest.search();
//        System.out.println(bfsTest);
//        bfsTest.printNodesInVisitedOrder();

        DijkstraShortestPath dspTest = new DijkstraShortestPath(graph, "Haven", "Violetplantsoen");
        dspTest.search();
        System.out.println(dspTest);
        dspTest.printNodesInVisitedOrder();
//
        A_Star aStarTest = new A_Star(graph, "Haven", "Violetplantsoen");
        aStarTest.search();
        System.out.println(aStarTest);
        aStarTest.printNodesInVisitedOrder();
    }
}
