package model;

public class Location {
    int x,y;
    public Location(int x ,int y){
        this.x =x;
        this.y=y;
    }
    public double travelTime( Location to){
        double xDifference = Math.abs(this.x - to.x);
        double yDifference = Math.abs(this.y - to.y);
        double totalDifference = xDifference + yDifference;
        return totalDifference * 1.5;
    }
}
