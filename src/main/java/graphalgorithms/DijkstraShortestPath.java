package graphalgorithms;

import model.Connection;
import model.IndexMinPQ;
import model.Station;
import model.TransportGraph;

import java.util.ArrayList;
import java.util.Stack;


public class DijkstraShortestPath extends AbstractPathSearch {
    protected Connection[] edgeTo;
    public DijkstraShortestPath(TransportGraph graph, String start, String end) {
        super(graph, start, end);
        nodesInPath = new ArrayList<Station>();
        edgeTo = new Connection[graph.getNumberOfStations()];
        distTo = new double[graph.getNumberOfStations()];
        pq = new IndexMinPQ<>(graph.getNumberOfStations());
        for (int v = 0; v < graph.getNumberOfStations(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
        distTo[startIndex] = 0.0;
        pq.insert(startIndex, 0.0);
    }

    @Override
    public void search() {
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            if(v == endIndex) {
                break;
            }
            relax(graph, v);
        }
        pathTo(endIndex);
        nodesInPath.add(graph.getStation(endIndex));
    }

    private double[] distTo;
    private IndexMinPQ<Double> pq;

    private void relax(TransportGraph G, int v) {
        for (int connectionId : G.getAdjacentVertices(v)) {
            Connection edge = graph.getConnection(v, connectionId);

            int w = graph.getIndexOfStationByName(edge.getTo().getStationName());

            if (distTo[w] > distTo[v] + edge.getWeight())
            {
                distTo[w] = distTo[v] + edge.getWeight();
                edgeTo[w] = edge;
                nodesVisited.add(graph.getStation(w));

                if (pq.contains(w)) {
                    pq.change(w, distTo[w]);
                } else {
                    pq.insert(w, distTo[w]);
                }
            }
        }
    }

    public double distTo(int v) {
        return distTo[v];
    }
    @Override
    public void pathTo(int v)
    {
        if (!hasPathTo(v)) return;
        Stack<Station> path = new Stack<>();
        for (Connection e = edgeTo[v]; e != null; e = edgeTo[graph.getIndexOfStationByName(e.getFrom().getStationName())]) {
            path.push(e.getFrom());
        }
        int size = path.size();
        for(int i =0; i<size ;i++){
            nodesInPath.add(path.pop());
        }
    }

    @Override
    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

}

