package graphalgorithms;

import model.Station;
import model.TransportGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstPath extends AbstractPathSearch{


    public BreadthFirstPath(TransportGraph graph, String start, String end) {
        super(graph, start, end);
    }
    @Override
    public void search() {
        verticesInPath = new LinkedList<Integer>();
        nodesInPath = new ArrayList<Station>();
        Queue<Integer> queue = new LinkedList<Integer>();
        marked[startIndex] = true; // Mark the source
        queue.add(startIndex); // and put it on the queue.
        nodesVisited.add(graph.getStation(startIndex));
        while (!queue.isEmpty())
        {
            int v = queue.remove(); // Remove next vertex from the queue.

            for (int w : graph.getAdjacentVertices(v))
                if (!marked[w]) // For every unmarked adjacent vertex,
                {
                    edgeTo[w] = v; // save last edge on a shortest path,
                    marked[w] = true; // mark it because path is known,
                    queue.add(w); // and add it to the queue.
                    nodesVisited.add(graph.getStation(w));
                    if(w==endIndex) {
                        super.pathTo(w);
                    }
                }
        }
    }

}
