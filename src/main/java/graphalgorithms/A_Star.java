package graphalgorithms;

import model.*;

import java.util.ArrayList;
import java.util.Stack;

public class A_Star extends AbstractPathSearch {
    protected Connection[] edgeTo;
    protected   double predictedDistance;
    public A_Star(TransportGraph graph, String start, String end) {
        super(graph, start, end);
        nodesInPath = new ArrayList<Station>();
        edgeTo = new Connection[graph.getNumberOfStations()];
        distTo = new double[graph.getNumberOfStations()];
        pq = new IndexMinPQ<>(graph.getNumberOfStations());
        for (int v = 0; v < graph.getNumberOfStations(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
        distTo[startIndex] = 0.0;
        pq.insert(startIndex, 0.0);

        Station startStation = graph.getStation(startIndex);
        Station endStation = graph.getStation(endIndex);
        predictedDistance = startStation.getLocation().travelTime(endStation.getLocation());
    }

    @Override
    public void search() {
        while (!pq.isEmpty()) {
            int v = pq.delMin();
            if (v == endIndex) {
                break;
            }
            nodesVisited.add(graph.getStation(v));
            relax(graph, v);
        }
        pathTo(endIndex);
        nodesInPath.add(graph.getStation(endIndex));
    }

    private double[] distTo;
    private IndexMinPQ<Double> pq;

    private void relax(TransportGraph G, int v) {
        for (int connectionId : G.getAdjacentVertices(v)) {
            Connection edge = graph.getConnection(v, connectionId);

            int w = graph.getIndexOfStationByName(edge.getTo().getStationName());

            if (distTo[w] > distTo[v] + edge.getWeight()) {
                distTo[w] = distTo[v] + edge.getWeight();
                edgeTo[w] = edge;

                if (pq.contains(w)) {
                    pq.change(w, distTo[w]+predictedDistance);
                } else {
                    pq.insert(w, distTo[w]+predictedDistance);
                }
            }
        }
    }

    public double distTo(int v) {
        return distTo[v];
    }

    @Override
    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    @Override
    public void pathTo(int v) {
        if (!hasPathTo(v)) return;
        Stack<Station> path = new Stack<>();
        for (Connection e = edgeTo[v]; e != null; e = edgeTo[graph.getIndexOfStationByName(e.getFrom().getStationName())]) {
            path.push(e.getFrom());
        }
        int size = path.size();
        for (int i = 0; i < size; i++) {
            nodesInPath.add(path.pop());
        }
    }
}
