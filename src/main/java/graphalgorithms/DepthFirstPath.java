package graphalgorithms;

import model.Station;
import model.TransportGraph;

import java.util.ArrayList;
import java.util.LinkedList;

public class DepthFirstPath extends  AbstractPathSearch {

    public DepthFirstPath(TransportGraph graph, String start, String end) {
        super(graph, start, end);
    }

    @Override
    public void search() {
        verticesInPath = new LinkedList<Integer>();
        nodesInPath = new ArrayList<Station>();
        DepthFirstSearch(startIndex);
        super.pathTo(endIndex);
    }

    private void DepthFirstSearch(int vertex) {
        marked[vertex] = true;
        nodesVisited.add(graph.getStation(vertex));

        for (int w : graph.getAdjacentVertices(vertex)) {

            if (!marked[w]) {
                edgeTo[w] = vertex;
                    DepthFirstSearch(w);

            }
        }
    }

}
